FROM node:15 as builder

RUN mkdir /app
WORKDIR /app

# Copy data and use prod config
COPY package*.json ./
COPY /src ./src
COPY /angular.json ./angular.json
COPY /tsconfig.app.json ./tsconfig.app.json
COPY /tsconfig.json ./tsconfig.json
COPY /tsconfig.spec.json ./tsconfig.spec.json
COPY /tslint.json ./tslint.json

# add `/app/node_modules/.bin` to $PATH
ENV PATH /app/node_modules/.bin:$PATH

# Build it
RUN npm ci --only=production

# Library has prod issues so in prod version we must patch it, see https://github.com/pa7/heatmap.js/issues/219
COPY /fix/heatmap.js ./node_modules/heatmap.js/build/heatmap.js
COPY /fix/heatmap.min.js ./node_modules/heatmap.js/build/heatmap.min.js

# Build it
RUN ng build --prod

# NGINX server
FROM nginx:stable-alpine
# Allow connect to server from outer world
EXPOSE 80
# And copy built data there
RUN rm /etc/nginx/conf.d/default.conf
COPY nginx/default.conf /etc/nginx/conf.d
COPY --from=builder /app/dist/vision-ui /usr/share/nginx/html

CMD ["nginx", "-g", "daemon off;"]
