import {IPoint} from './IPoint';

/**
 * Numerical rounding
 */
export enum Rounding {
  UP,
  DOWN,
  NONE
}

/**
 * Mapping of one number to another
 */
type Conversion = (number: number) => number

/**
 * Rounds number depending on selected {@param rounding}
 */
function round(rounding: Rounding): Conversion {
  switch (rounding) {
    case Rounding.DOWN:
      return Math.floor;
    case Rounding.UP:
      return Math.ceil;
    case Rounding.NONE:
      return (x) => {
        return x;
      };
    default:
      return null;
  }
}

export class Point implements IPoint {

  private readonly _x: number;
  private readonly _y: number;

  constructor(x: number, y: number) {
    this._x = x;
    this._y = y;
  }

  get x() {
    return this._x;
  }

  get y() {
    return this._y;
  }

  divide(other: number, rounding: Rounding = Rounding.NONE): Point {
    const f = round(rounding);
    return new Point(f(this.x / other), f(this.y / other));
  }

  multiply(other: number): Point {
    return new Point(this.x * other, this.y * other);
  }

  minus(other: IPoint) {
    return new Point(this.x - other.x, this.y - other.y);
  }

  mean(other: IPoint, rounding: Rounding = Rounding.NONE) {
    const f = round(rounding);
    return new Point(f((this.x + other.x) / 2), f((this.y + other.y) / 2));
  }

  equals(other: any): boolean {
    if (other instanceof Point) {
      return this.x === other.x && this.y === other.y;
    } else {
      return false;
    }
  }


  toString(): string {
    return `Point{x=${this.x}, y=${this.y}}`;
  }
}
