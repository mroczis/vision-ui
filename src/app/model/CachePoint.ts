import {Rounding} from './Point';
import {IPoint} from './IPoint';

/**
 * [X,Y] point that has time when it was created and which image
 * was shown at that point.
 */
export class CachePoint implements IPoint {

  private readonly _point: IPoint;
  private readonly _timestamp: number;
  private readonly _requestId: number | null;

  constructor(point: IPoint, requestId: number | null = null, timestamp: number = Date.now()) {
    this._point = point;
    this._requestId = requestId;
    this._timestamp = timestamp;
  }

  get point(): IPoint {
    return this._point;
  }

  get timestamp(): number {
    return this._timestamp;
  }

  get requestId() : number | null {
    return this._requestId;
  }

  get x(): number {
    return this._point.x;
  }

  get y(): number {
    return this._point.y;
  }

  divide(other: number, rounding: Rounding = Rounding.NONE): CachePoint {
    return this.copy(this.point.divide(other, rounding));
  }

  mean(other: IPoint, rounding: Rounding = Rounding.NONE): CachePoint {
    return this.copy(this.point.mean(other, rounding));
  }

  minus(other: IPoint): CachePoint {
    return this.copy(this.point.minus(other));
  }

  multiply(other: number): CachePoint {
    return this.copy(this.point.multiply(other));
  }

  private copy(
    point: IPoint = this.point,
    requestId: number = this.requestId,
    timestamp: number = this.timestamp
  ) : CachePoint {
    return new CachePoint(point, requestId, timestamp)
  }

}
