/**
 * Consumable prevents multiple consumptions of same item in reactive streams
 */
export class Consumable<T> {

  private consumed: boolean = false;
  private readonly t: T;

  constructor(t: T, consumed: boolean = false) {
    this.t = t;
    this.consumed = consumed;
  }

  consume(block: (t: T) => any) {
    if (!this.consumed) {
      this.consumed = true;
      block(this.t);
    }
  }

}
