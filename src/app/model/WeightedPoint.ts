import {DataPoint} from 'heatmap.js';

/**
 * 2D point that has some weight. Used for heat maps.
 */
export class WeightedPoint implements DataPoint {

  private readonly _x: number;
  private readonly _y: number;
  private readonly _value: number;

  constructor(x: number, y: number, value: number) {
    this._x = x;
    this._y = y;
    this._value = value
  }

  get x() {
    return this._x;
  }

  get y() {
    return this._y;
  }

  get value() {
    return this._value;
  }
}
