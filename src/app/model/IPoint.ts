import {Rounding} from './Point';

/**
 * Generic 2D point
 */
export interface IPoint {
  readonly x: number;
  readonly y: number;

  minus(other: IPoint): IPoint;
  divide(other: number, rounding?: Rounding) : IPoint;
  multiply(other: number): IPoint;
  mean(other: IPoint, rounding?: Rounding): IPoint
}
