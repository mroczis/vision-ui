import {ScanType} from '../enums/ScanType';

/**
 * Generic request for scanning
 */
export interface ScanRequest {
  /**
   * Id of request, null when request is locally created (not from bundle)
   */
  id(): number | null

  /**
   * Source path or content suitable for HTML img element
   */
  src(): string | null

  /**
   * Type
   */
  type(): ScanType

  /**
   * Name of item, null when request is locally created (not from bundle)
   */
  name() : string | null
}

/**
 * Image URL wrapper
 */
export class UrlImageScanRequest implements ScanRequest {

  private readonly _url: string;
  private readonly _id: number | null;
  private readonly _name: string | null;

  constructor(
    url: string,
    id: number | null = null,
    name: string | null = null
  ) {
    this._url = url;
    this._id = id;
    this._name = name;
  }

  src(): string {
    return this.url;
  }

  id(): number | null {
    return this._id;
  }

  type(): ScanType {
    return ScanType.IMAGE_URL;
  }

  get url(): string {
    return this._url;
  }

  name() : string | null {
    return this._name;
  }

  toString(): string {
    return `UrlImageScanRequest(id = ${this._id}, url = ${this._url}, name = ${this._name})`;
  }

}

/**
 * Base64 image data wrapper
 */
export class FileImageScanRequest implements ScanRequest {

  private readonly _content: string;
  private readonly _name: string;
  private readonly _id: number | null;

  constructor(
    content: string,
    name: string,
    id: number | null = null,
  ) {
    this._content = content;
    this._name = name;
    this._id = id;
  }

  src(): string {
    return this.content;
  }

  id(): number | null {
    return this._id;
  }

  type(): ScanType {
    return ScanType.IMAGE_FILE;
  }

  get content(): string {
    return this._content;
  }

  name(): string {
    return this._name;
  }

  toString(): string {
    return `FileImageScanRequest(id = ${this._id}, name = ${this._name})`;
  }

}

/**
 * Website image wrapper
 */
export class WebScanRequest implements ScanRequest {

  private readonly _url: string;
  private readonly _id: number | null;
  private readonly _name: string | null;

  constructor(
    url: string,
    id: number | null = null,
    name: string | null = null
  ) {
    this._url = url;
    this._id = id;
    this._name = name;
  }

  get url(): string {
    return this._url;
  }

  src(): string {
    return this.url;
  }

  id(): number | null {
    return this._id;
  }

  type(): ScanType {
    return ScanType.WEBSITE;
  }

  name() : string | null {
    return this._name;
  }

  toString(): string {
    return `WebScanRequest(id = ${this._id}, url = ${this._url}, name = ${this._name})`;
  }
}
