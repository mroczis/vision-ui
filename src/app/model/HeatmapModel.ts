import {WeightedPoint} from './WeightedPoint';
import {HeatmapData} from 'heatmap.js';

/**
 * Heat map model as specified by underlying library
 */
export class HeatmapModel implements HeatmapData<WeightedPoint> {

  private readonly _min: number;
  private readonly _max: number;
  private readonly _points: Array<WeightedPoint>;

  constructor(points: Array<WeightedPoint>, max: number, min: number = 0) {
    this._min = min;
    this._max = max;
    this._points = points;
  }

  get max(): number {
    return this._max;
  }

  get min(): number {
    return this._min;
  }

  get data(): ReadonlyArray<WeightedPoint> {
    return this._points;
  }

}
