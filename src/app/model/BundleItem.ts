export class BundleItem {

    readonly id: number
    readonly name: string
    readonly url: string

    constructor(
        id: number,
        name: string,
        url: string
    ) {
        this.id = id;
        this.url = url;
        this.name = name;
    }

    copy(
        id: number = this.id,
        name: string = this.name,
        url: string = this.url
    ) : BundleItem {
        return new BundleItem(id, name, url)
    }

}
