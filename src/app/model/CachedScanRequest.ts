import {ScanRequest} from './ScanRequest';
import {ScanType} from '../enums/ScanType';
import {Rect} from './Rect';

/**
 * Just like ScanRequest but this has img element that is pre-cached in order to speed up the app.
 */
export class CachedScanRequest implements ScanRequest {

  /**
   * Image element holding instance of bitmap that is pre-cached here
   */
  private readonly _image: HTMLImageElement;

  /**
   * Original model
   */
  private readonly _request: ScanRequest;

  /**
   * Position relative to whole canvas, can be used to cut correct part of gaze point
   */
  private _clipBounds: Rect | null = null;

  constructor(image: HTMLImageElement, request: ScanRequest) {
    this._image = image;
    this._request = request;
  }

  id(): number {
    return this._request.id();
  }

  src(): string | null {
    return this._image.src;
  }

  type(): ScanType {
    return this._request.type();
  }

  name(): string | null {
    return this._request.name();
  }

  get image() : HTMLImageElement {
    return this._image;
  }

  get original(): ScanRequest {
    return this._request;
  }

  set clipBounds(rect: Rect) {
    this._clipBounds = rect;
  }

  get clipBounds(): Rect | null {
    return this._clipBounds;
  }
}
