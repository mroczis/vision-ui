import {Bundle} from './Bundle';

/**
 * Bundle that is about to be saved on server
 */
export interface OutgoingBundle {
  name: string
  key: string
  items: Array<OutgoingBundleItem>
}

/**
 * Bundle item that is about to be saved on server
 */
export interface OutgoingBundleItem {
  name: string
  url: string
  isStart: boolean
}

/**
 * Converts bundle to server representation model
 */
export function toOutgoingBundle(bundle: Bundle) : OutgoingBundle {
  return {
    name: bundle.name,
    key: bundle.key,
    items: bundle.items.map(it => {
      return {
        name: it.name,
        url: it.url,
        isStart: it.id == bundle.startItemId
      }
    })
  }
}
