import {Bundle} from './Bundle';

/**
 * Incoming response from server when querying for all bundles
 */
export class PagedBundle {
  readonly bundles: Array<Bundle>
  readonly count: number

  constructor(bundles: Array<Bundle>, count: number) {
    this.bundles = bundles
    this.count = count
  }
}
