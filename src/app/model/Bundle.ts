import {BundleItem} from "./BundleItem";

export class Bundle {

    readonly id: number
    readonly name: string
    readonly key: string
    readonly startItemId: number
    readonly items: Array<BundleItem>

    constructor(
        id: number,
        name: string,
        key: string,
        startItemId: number,
        items: Array<BundleItem>
    ) {
        this.id = id
        this.name = name
        this.key = key
        this.items = items
        this.startItemId = startItemId
    }

    copy(
        id: number = this.id,
        name: string = this.name,
        key: string = this.key,
        startItemId: number = this.startItemId,
        items: Array<BundleItem> = this.items,
    ): Bundle {
        return new Bundle(id, name, key, startItemId, items)
    }
}
