import {Component, ElementRef, NgZone, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import {MatSnackBar} from '@angular/material/snack-bar';
import {GazeService} from '../../services/gaze/gaze.service';
import {GazeScanningState} from '../../enums/GazeScanningState';
import {DialogFinishComponentDialog} from '../dialog/dialog-finish/dialog-finish-component-dialog.component';
import {MatDialog} from '@angular/material/dialog';
import {Rect} from '../../model/Rect';
import {ScanService} from '../../services/scan/scan.service';
import {ApiService} from '../../services/api/api.service';
import {Route} from '../../enums/Route';
import {Observable, Subscription} from 'rxjs';
import {CachedScanRequest} from '../../model/CachedScanRequest';
import {ScanningState} from '../../enums/ScanningState';
import {map} from 'rxjs/operators';

/**
 * Scan component renders current image and optionally side menu with
 * options to change the image.
 */
@Component({
  selector: 'app-scan',
  templateUrl: './scan.component.html',
  styleUrls: ['./scan.component.css']
})
export class ScanComponent implements OnInit, OnDestroy {

  model: CachedScanRequest;
  data: string = '';

  @ViewChild('image') set imageRef(imageRef: ElementRef) {
    this.image = <HTMLImageElement> imageRef.nativeElement;
    this.image.addEventListener('load', (_) => {
      this.repositionImage(this.container, this.image);
      this.gaze.imageLoaded = true;
    });
    this.image.addEventListener('error', (e) => {
      if (this.data.length > 0) {
        console.log(e);
        this.image.style.display = 'none';
        const ref = this._snackBar.open('Provided URL address or file does not point to valid image', 'Go back', {
          duration: Number.MAX_VALUE
        });
        ref.onAction().subscribe(() => {
          this.router.navigate([Route.DASHBOARD], {replaceUrl: true});
        });
      }
    });
  };

  @ViewChild('container') set containerRef(containerRef: ElementRef) {
    this.container = <HTMLDivElement> containerRef.nativeElement;
  }

  image: HTMLImageElement;
  container: HTMLDivElement;
  imagePosition: Rect | null;
  scanRequestSubscription: Subscription;
  scanItems: Array<CachedScanRequest>;
  renderMenu: Boolean = false;
  showMenu: Observable<Boolean>;
  showStop: Observable<Boolean>;
  showPause: Observable<Boolean>;

  constructor(
    public gaze: GazeService,
    private router: Router,
    private _snackBar: MatSnackBar,
    private scan: ScanService,
    private api: ApiService,
    private dialog: MatDialog,
    private ngZone: NgZone
  ) {
    this.scanRequestSubscription = this.scan.scanRequestObservable.subscribe((model) => {
      ngZone.run(() => {
        if (model === null) {
          router.navigate([Route.DASHBOARD]);
        } else {
          this.model = model;
          this.data = model.src();
        }
      });
    });

    this.scanItems = this.scan.scanRequestArray;
    this.renderMenu = this.scanItems.length > 1;
    this.showMenu = gaze.state.pipe(
      map((it) => it == ScanningState.SCANNING)
    );

    this.showPause = gaze.state.pipe(
      map((it) => it == ScanningState.SCANNING || it == ScanningState.FINISHED)
    );

    this.showStop = gaze.state.pipe(
      map((it) => it == ScanningState.SCANNING || it == ScanningState.PAUSED || it == ScanningState.FINISHED)
    );
  }

  ngOnInit(): void {

  }

  ngOnDestroy(): void {
    this.gaze.imageLoaded = false;
    this.scanRequestSubscription.unsubscribe();
  }

  onResize(_: Event) {
    if (this.container !== undefined && this.image !== undefined) {
      this.repositionImage(<HTMLDivElement> this.container, this.image);
    }
  }

  private repositionImage(container: HTMLDivElement, image: HTMLImageElement) {
    const position = this.scan.calculateImagePosition(
      container.clientWidth,
      window.innerHeight,
      image.naturalWidth,
      image.naturalHeight,
      container.offsetTop,
      container.offsetLeft,
    );

    const left = Math.max(0, position.left - container.offsetLeft);
    const top = Math.max(0, position.top - container.offsetTop);

    image.width = position.width;
    image.height = position.height;
    image.style.marginLeft = `${left}px`;
    image.style.marginTop = `${top}px`;

    if (this.model !== null) {
      this.model.clipBounds = position;
    }

    this.imagePosition = position;
  }

  onCancel() {
    this.router.navigate([Route.DASHBOARD], {replaceUrl: true});
  }

  onPause() {
    this.gaze.scanningState = GazeScanningState.PAUSED;
  }

  onDone() {
    this.gaze.scanningState = GazeScanningState.FINISHED;

    const dialogRef = this.dialog.open(DialogFinishComponentDialog, {
      width: '440px',
    });

    dialogRef.afterClosed().subscribe(result => {
      this.gaze.scanningState = GazeScanningState.READY;
      this.scan.scanRequest = null;
      this.router.navigate([Route.DASHBOARD]);
    });
  }

  onSelectItem(item: CachedScanRequest) {
    this.scan.requestId = item.id();
  }
}
