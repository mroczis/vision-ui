import {Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Route} from '../../../enums/Route';
import {AuthService} from '../../../services/auth/auth.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Bundle} from '../../../model/Bundle';
import {map, mergeMap} from 'rxjs/operators';
import {BehaviorSubject, Observable, Subscription} from 'rxjs';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {BundleItem} from '../../../model/BundleItem';
import {MatSelectChange} from '@angular/material/select';
import {toOutgoingBundle} from '../../../model/OutgoingBundle';
import {NotificationService} from '../../../services/auth/notification.service';
import {urlValidator} from '../../../util/ValidatorsExt';

@Component({
  selector: 'app-bundle',
  templateUrl: './bundle.component.html',
  styleUrls: ['./bundle.component.css']
})
export class BundleComponent implements OnInit, OnDestroy {

  @ViewChild('name') nameInput: ElementRef;
  @ViewChild('key') keyInput: ElementRef;
  @ViewChild('images') imagesInput: ElementRef;

  private bundleSub: Subscription;
  private bundleNameControl = new FormControl('', [Validators.required]);
  private bundleKeyControl = new FormControl('', [Validators.required]);
  private startItemControl = new FormControl('', [Validators.required]);

  /**
   * Bundle to create, edit, delete, ...
   */
  bundle: Bundle | null = null;
  /**
   * Id of image that should be used as default when starting scan
   */
  selectedImage: string | null = null;
  disableSelect = new FormControl(false);

  /**
   * Validation object
   */
  form = new FormGroup({
    bundleName: this.bundleNameControl,
    bundleKey: this.bundleKeyControl,
    startItem: this.startItemControl,
  });

  constructor(
    private auth: AuthService,
    private router: Router,
    private route: ActivatedRoute,
    private notification: NotificationService,
  ) {
    if (auth.baseAuthHeader == null) {
      router.navigate([Route.ADMIN_LOGIN]);
    }
  }

  ngOnInit(): void {
    this.bundleSub = this.route.params.pipe(
      map(params => params['key']),
      mergeMap(key => {
        if (key === '') {
          const id = this.randomId()
          return new BehaviorSubject(new Bundle(
            -1,
            '',
            '',
            id,
            new Array<BundleItem>(new BundleItem(id, '', ''))
          )).asObservable();
        } else {
          return this.auth.getBundle(key);
        }
      })
    ).subscribe((bundle) => {
      bundle.items.forEach(item => this.ensureFormControl(item));
      this.bundleNameControl.setValue(bundle.name);
      this.bundleKeyControl.setValue(bundle.key);

      const selectedImage = bundle.items.find((item) => {
        return item.id == bundle.startItemId;
      });
      this.selectedImage = this.getSelectedOption(selectedImage?.id);
      this.startItemControl.setValue(this.selectedImage);
      this.bundle = bundle;
    });

  }

  ngOnDestroy(): void {
    if (this.bundleSub) {
      this.bundleSub.unsubscribe();
    }
  }

  onLogout() {
    this.auth.logout();
    this.router.navigate([Route.ADMIN_LOGIN]);
  }

  onHomeClick() {
    this.router.navigate([Route.ADMIN_BUNDLES]);
  }

  onCancel() {
    this.router.navigate([Route.ADMIN_BUNDLES]);
  }

  onSave() {
    this.form.markAllAsTouched();
    console.log(this.form)

    if (this.form.status !== "INVALID") {
      const bundle = this.getBundle();
      let observable: Observable<Bundle>;
      if (bundle.id == -1) {
        observable = this.auth.createBundle(toOutgoingBundle(bundle));
      } else {
        observable = this.auth.updateBundle(bundle.id, toOutgoingBundle(bundle));
      }

      const sub = observable.subscribe(() => {
        sub.unsubscribe();
        this.notification.setMessage("Bundle has been saved")
        this.router.navigate([Route.ADMIN_BUNDLES]);
      });

    }
  }

  onDeleteBundle() {
    if (this.bundle.id != -1) {
      const action = this.auth.deleteBundle(this.bundle.id);
      const sub = action.subscribe(res => {
        sub.unsubscribe();
        this.notification.setMessage("Bundle has been deleted")
        this.router.navigate([Route.ADMIN_BUNDLES]);
      });
    } else {
      this.notification.setMessage("Bundle discarded")
      this.router.navigate([Route.ADMIN_BUNDLES]);
    }
  }

  onDelete(item: BundleItem) {
    if (this.bundle.items.length == 1) {
      // Remove current item and generate a new one
      const b = this.bundle;
      const id = this.randomId();
      const item = new BundleItem(id, '', '');
      this.bundle = new Bundle(b.id, b.name, b.key, b.startItemId, [item]);
      const newVal = this.getSelectedOption(id);

      this.ensureFormControl(item);
      this.selectedImage = newVal;
      this.startItemControl.setValue(newVal);
    } else {
      // Remove item
      const b = this.bundle;
      const items = b.items.filter((it) => it != item);
      this.bundle = new Bundle(b.id, b.name, b.key, b.startItemId, items);

      // Pick different one as selected one
      if (this.getSelectedOption(item.id) == this.selectedImage) {
        const newId = this.bundle.items[0]?.id;
        if (newId) {
          const newVal = this.getSelectedOption(this.bundle.items[0]?.id);
          this.selectedImage = newVal;
          this.startItemControl.setValue(newVal);
        } else {
          this.selectedImage = '';
          this.startItemControl.setValue('');
        }
      }
    }
  }

  onAddImage() {
    const b = this.bundle;
    const items: Array<BundleItem> = [];
    const item = new BundleItem(this.randomId(), '', '');
    b.items.forEach((it) => items.push(it));
    items.push(item);
    this.ensureFormControl(item);

    this.bundle = new Bundle(b.id, b.name, b.key, b.startItemId, items);
  }

  /**
   * Emitted by JS when name of any item changes
   */
  onNameChanged() {
    this.bundle = this.getBundle();
  }

  /**
   * Emitted by JS, event value represents local key in form `option-[id]`
   */
  onStartItemChanged(event: MatSelectChange) {
    this.selectedImage = event.value;
  }

  /**
   * Helper function that helps JS keep field focused when data change (when we force
   * redrawing after
   */
  trackByFn(index: any, item: any) {
    return index;
  }

  /**
   * Creates bundle from current form.
   */
  private getBundle() : Bundle {
    const name = (<HTMLInputElement> this.nameInput.nativeElement).value;
    const key = (<HTMLInputElement> this.keyInput.nativeElement).value;
    const imageNode = (<HTMLDivElement> this.imagesInput.nativeElement);
    const startItem = Number.parseInt(this.selectedImage.replace('option-', ''));

    const images = new Array<BundleItem>();
    imageNode.childNodes.forEach((it) => {
      if (it instanceof HTMLDivElement) {
        const id = Number.parseInt(it.id.replace('image-', ''));
        const nameId = `${it.id}-name`;
        const urlId = `${it.id}-url`;
        const nameElement = <HTMLInputElement> document.getElementById(nameId);
        const urlElement = <HTMLInputElement> document.getElementById(urlId);

        images.push(new BundleItem(id, nameElement.value, urlElement.value));
      }
    });

    return new Bundle(this.bundle.id, name, key, startItem, images);
  }

  /**
   * Generates random ID
   */
  private randomId(): number {
    return Math.floor(Math.random() * Number.MAX_SAFE_INTEGER);
  }

  /**
   * Generates id of selected image from its id
   */
  private getSelectedOption(id: number | null | undefined): string | null {
    if (id) {
      return `option-${id}`;
    } else {
      return null;
    }
  }

  /**
   * Generated form control components for validation for images
   * that are appended/deleted dynamically.
   */
  private ensureFormControl(item: BundleItem) {
    if (this.form.contains(`imageName${item.id}`) === false) {
      this.form.addControl(
        `imageName${item.id}`,
        new FormControl(item.name, [Validators.required])
      );
    }

    if (this.form.contains(`imageUrl${item.id}`) === false) {
      this.form.addControl(
        `imageUrl${item.id}`,
        new FormControl(item.url, [Validators.required, urlValidator()])
      );
    }
  }

}
