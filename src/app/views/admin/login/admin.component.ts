import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../../../services/auth/auth.service';
import {Router} from '@angular/router';
import {Route} from '../../../enums/Route';

/**
 * Login into administration component.
 */
@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {

  @ViewChild('username') username: ElementRef;
  @ViewChild('password') password: ElementRef;

  userForm = new FormGroup({
    username: new FormControl('', [Validators.required]),
    password: new FormControl('', [Validators.required])
  });

  constructor(
    private authService: AuthService,
    private router: Router,
  ) {
  }

  ngOnInit(): void {
  }

  onLoginClicked() {
    this.userForm.markAllAsTouched();
    const username = <FormControl> this.userForm.get('username');
    const password = <FormControl> this.userForm.get('password');
    if ((username.errors == null || username.errors.length == 0) && (password.errors == null || password.errors.length == 0)) {
      const usernameVal = this.username.nativeElement.value
      const passwordVal = this.password.nativeElement.value

      this.authService.login(usernameVal, passwordVal).toPromise().then((isOk) => {
        if (isOk) {
          this.router.navigate([Route.ADMIN_BUNDLES]);
        } else {
          password.setErrors({wrongPassword: true})
        }
      })
    }
  }

}
