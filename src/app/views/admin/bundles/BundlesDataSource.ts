import {CollectionViewer, DataSource} from '@angular/cdk/collections';
import {Bundle} from '../../../model/Bundle';
import {BehaviorSubject, Observable, of} from 'rxjs';
import {AuthService} from '../../../services/auth/auth.service';
import {catchError, finalize} from 'rxjs/operators';
import {PagedBundle} from '../../../model/PagedBundle';
import {Router} from '@angular/router';
import {Route} from '../../../enums/Route';

/**
 * Data source that is able to handle pagination of bundles
 */
export class BundlesDataSource implements DataSource<Bundle> {

  private bundles = new BehaviorSubject<Bundle[]>([]);
  private total = new BehaviorSubject<number>(0);
  private loading = new BehaviorSubject<boolean>(false);

  pages$ : Observable<number> = this.total.asObservable()

  constructor(
    private auth: AuthService,
    private router: Router
  ) {
  }

  connect(collectionViewer: CollectionViewer): Observable<Bundle[] | ReadonlyArray<Bundle>> {
    return this.bundles.asObservable();
  }

  disconnect(collectionViewer: CollectionViewer): void {
    this.loading.complete();
    this.bundles.complete();
  }


  load(limit: number, offset: number, query: string) {
    this.loading.next(true);

    this.auth.getBundles(limit, offset, query).pipe(
      catchError(() => {
        this.router.navigate([Route.ADMIN_LOGIN]);
        return of(new PagedBundle([], 0))
      }),
      finalize(() => this.loading.next(false))
    ).subscribe(pagedBundle => {
      this.bundles.next(pagedBundle.bundles)
      this.total.next(pagedBundle.count)
    });
  }

}
