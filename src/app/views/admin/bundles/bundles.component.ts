import {AfterViewInit, Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {AuthService} from '../../../services/auth/auth.service';
import {Router} from '@angular/router';
import {Route} from '../../../enums/Route';
import {MatPaginator} from '@angular/material/paginator';
import {BundlesDataSource} from './BundlesDataSource';
import {map, tap} from 'rxjs/operators';
import {Bundle} from '../../../model/Bundle';
import {Observable, Subscription} from 'rxjs';
import {NotificationService} from '../../../services/auth/notification.service';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
  selector: 'app-bundles',
  templateUrl: './bundles.component.html',
  styleUrls: ['./bundles.component.css']
})
export class BundlesComponent implements AfterViewInit, OnInit {

  private readonly DEFAULT_PAGE_SIZE : number = 20
  private messageSub: Subscription;

  @ViewChild('search') search: ElementRef;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  /**
   * Data source giving us data for the UI
   */
  dataSource : BundlesDataSource

  /**
   * Total amount of pages
   */
  pageCount : Observable<number>

  displayedColumns: string[] = ['name', 'key', 'count'];

  /**
   * User input for filtration
   */
  query: string = '';

  constructor(
    private auth: AuthService,
    private router: Router,
    private notification: NotificationService,
    private snackBar: MatSnackBar,
  ) {
  }

  onLogout() {
    this.auth.logout();
    this.router.navigate([Route.ADMIN_LOGIN]);
  }

  /**
   * Reset pagination and lead data when search changes
   */
  onSearchChanged() {
    this.query = this.search.nativeElement.value;
    this.paginator.pageIndex = 0

    const offset = this.paginator.pageIndex * this.paginator.pageSize
    const limit = this.paginator.pageSize
    this.dataSource.load(limit, offset, this.query);
  }

  ngOnInit(): void {
    this.dataSource = new BundlesDataSource(this.auth, this.router);
    this.dataSource.load(this.DEFAULT_PAGE_SIZE,0, "");
    this.pageCount = this.dataSource.pages$.pipe(
      map((count) => {
        if (this.paginator) {
          return Math.max(1, Math.floor(count / this.paginator.pageSize))
        } else {
          return Math.max(1, Math.floor(count / this.DEFAULT_PAGE_SIZE))
        }
      })
    )

    this.messageSub = this.notification.message.subscribe((consumable) => {
      consumable.consume((message) => {
        this.snackBar.open(message, null, {
          duration: 2000
        });
      })
    })
  }

  ngAfterViewInit() {
    this.paginator.page
      .pipe(tap(() => {
        const offset = this.paginator.pageIndex * this.paginator.pageSize
        const limit = this.paginator.pageSize
        this.dataSource.load(limit, offset, this.query);
      }))
      .subscribe();
  }

  onRowClicked(row: Bundle) {
    this.router.navigate([Route.ADMIN_BUNDLES, row.key])
  }

  onHomeClick() {
    this.router.navigate([Route.ADMIN_BUNDLES])
  }

  onCreteBundle() {
    this.router.navigate([Route.ADMIN_BUNDLES, ""])
  }
}
