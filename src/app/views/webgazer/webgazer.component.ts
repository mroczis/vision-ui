import {Component, HostListener, OnDestroy, OnInit} from '@angular/core';

import webgazer from 'webgazer';
import {ScanningState} from '../../enums/ScanningState';
import {Point} from '../../model/Point';
import {GazeService} from '../../services/gaze/gaze.service';
import {CalibrationService} from '../../services/calibration/calibration.service';
import {CacheService} from '../../services/cache/cache.service';
import {Observable, Subscription} from 'rxjs';

/**
 * Webgazer component isolates webgazer library from the rest of the system.
 * It collects data about gaze and saves them into cache.
 */
@Component({
  selector: 'app-webgazer',
  templateUrl: './webgazer.component.html',
  styleUrls: ['./webgazer.component.css']
})
export class WebgazerComponent implements OnInit, OnDestroy {

  mutationObserver: MutationObserver | null = null;
  currentStateObservable: Observable<ScanningState>;
  currentState: ScanningState = ScanningState.LOADING;
  scrollOffset: Point = new Point(0, 0);

  enabledHandle: Subscription | null = null;

  constructor(
    private service: GazeService,
    private calibration: CalibrationService,
    private cache: CacheService
  ) {
  }

  ngOnInit(): void {
    this.currentStateObservable = this.service.state;
    this.currentStateObservable.subscribe((it) => {
      this.currentState = it;

      if (it === ScanningState.PAUSED) {
        webgazer.pause();
      } else if (it === ScanningState.SCANNING) {
        webgazer.resume();
      }
    });

    this.enabledHandle = this.service.enabledObservable.subscribe((it) => {
      if (it) {
        this.create();
      } else {
        this.destroy();
      }
    });

    this.checkForFace();
  }

  ngOnDestroy(): void {
    this.destroy();
    if (this.enabledHandle !== null) {
      this.enabledHandle.unsubscribe();
    }
  }

  @HostListener('window:scroll', ['event'])
  onScroll(event: Event) {
    this.scrollOffset = new Point(window.pageXOffset, window.pageYOffset);
  }

  private create() {
    this.removeElements();
    const component = this;

    webgazer.params.showVideoPreview = true;
    webgazer.params.showFaceOverlay = false;
    webgazer
      .setRegression('ridge')
      .setGazeListener(function(data, clock) {
        component.checkForFace();

        if (data !== null && component.currentState === ScanningState.SCANNING) {
          const x = data.x + component.scrollOffset.x;
          const y = data.y + component.scrollOffset.y;

          component.cache.add(new Point(x, y));
        }
      }).begin();

    webgazer.showPredictionPoints(false);
  }

  private destroy() {
    if (this.mutationObserver !== null) {
      this.mutationObserver.disconnect();
    }

    try {
      webgazer.end();
    } catch (e) {
      // ignored, this might happen when webgazer is already turned off
    }
    webgazer.clearData();

    try {
      webgazer.stopVideo();
    } catch (e) {
      // ignored, this might happen when webgazer is already turned off
    }
    this.removeElements();

    this.service.webgazerLoaded = false;
    this.calibration.clear();
    this.cache.clear();
    this.mutationObserver = null;
  }

  private removeElements() {
    ['webgazerFaceOverlay', 'webgazerVideoFeed', 'webgazerFaceFeedbackBox', 'webgazerGazeDot', 'webgazerVideoCanvas'].forEach((id) => {
      const element = <HTMLElement> document.getElementById(id);
      if (element !== undefined && element !== null) {
        element.remove();
      }
    });
  }

  /**
   * This binds to Webgazer library which does not provide callback if face was detected or not.
   * We bind on HTML element that changes its color (green when face is detected, red when not) and will
   * make our own observable out of that
   */
  private checkForFace() {
    if (this.mutationObserver === null) {
      const element = <HTMLCanvasElement> document.getElementById('webgazerFaceFeedbackBox');
      const service = this.service;

      if (element !== null && element !== undefined) {
        const observer = new MutationObserver(function(mutations) {
          service.webgazerLoaded = true;
          mutations.forEach(() => {
            service.faceDetected = element.style.border.includes('green');
          });
        });

        observer.observe(element, {attributes: true, attributeFilter: ['style']});
        this.mutationObserver = observer;
      }
    }
  }

}
