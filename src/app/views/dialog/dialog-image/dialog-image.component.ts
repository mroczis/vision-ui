import {Component, ElementRef, Inject, OnDestroy, OnInit, ViewChild, ViewContainerRef} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {FileImageScanRequest, ScanRequest, UrlImageScanRequest} from '../../../model/ScanRequest';
import {urlValidator} from '../../../util/ValidatorsExt';
import {Router} from '@angular/router';
import {ScanService} from '../../../services/scan/scan.service';
import {GazeService} from '../../../services/gaze/gaze.service';
import {Route} from '../../../enums/Route';
import {ScanType} from '../../../enums/ScanType';
import {CachedScanRequest} from '../../../model/CachedScanRequest';
import {first, timeout} from 'rxjs/operators';
import {ApiService} from '../../../services/api/api.service';

/**
 * Enter image url of upload file button that will present dialog
 */
@Component({
  selector: 'app-dialog-image',
  templateUrl: './dialog-image.component.html',
  styleUrls: ['./dialog-image.component.css']
})
export class DialogImageComponent implements OnInit, OnDestroy {


  constructor(
    public dialog: MatDialog,
    private router: Router,
    private scan: ScanService,
    private gazeService: GazeService
  ) {
  }

  ngOnInit(): void {
    this.scan.scanRequestObservable.pipe(first(), timeout(100)).subscribe(
      (request) => {
        console.log(request);
        if (request !== null && (request.type() === ScanType.IMAGE_FILE || (request.type() === ScanType.IMAGE_URL && request.name() === null))) {
          this.openDialog(request);
        }
      },
      (_) => {
        // error ignored
      }
    );
  }

  ngOnDestroy(): void {
  }

  openDialog(request: CachedScanRequest | null = null): void {
    const dialogRef = this.dialog.open(DialogImageComponentDialog, {
      width: '440px',
      data: {request: request}
    });

    dialogRef.afterClosed().subscribe(result => {
      let validatedResult: ScanRequest | null = null;
      if (result instanceof FileImageScanRequest || result instanceof UrlImageScanRequest) {
        validatedResult = result;
      }

      this.scan.scanRequest = validatedResult;

      if (validatedResult !== null) {
        this.gazeService.enabled = true;
        this.router.navigate([Route.SCAN]);
      }
    });
  }
}


@Component({
  selector: 'app-dialog-image-dialog',
  templateUrl: './dialog-image.component-dialog.html',
  styleUrls: ['./dialog-image.component-dialog.css'],
})
// tslint:disable-next-line:component-class-suffix
export class DialogImageComponentDialog {

  selectedFiles: FileList | null = null;
  currentTab: number = 0;

  fileName: string | null = null;
  urlValue: string | null = null;

  urlForm = new FormGroup({
    urlControl: new FormControl('', [
      Validators.required,
      urlValidator()
    ]),
  });

  fileForm = new FormGroup({
    fileControl: new FormControl('', [
      Validators.required,
    ]),
  });

  @ViewChild('url') urlInput: ElementRef;
  @ViewChild('noFileError', {read: ViewContainerRef}) noFileError: ViewContainerRef;

  constructor(
    private dialogRef: MatDialogRef<DialogImageComponentDialog>,
    private api: ApiService,
    @Inject(MAT_DIALOG_DATA) public data: { request: CachedScanRequest | null }
  ) {
    let originalRequest: ScanRequest;
    if (data.request instanceof CachedScanRequest) {
      originalRequest = data.request.original;
    } else {
      originalRequest = data.request;
    }

    if (originalRequest !== null) {
      if (originalRequest instanceof FileImageScanRequest) {
        this.currentTab = 0;
        this.fileName = originalRequest.name();
        this.urlValue = null;
      } else if (originalRequest instanceof UrlImageScanRequest) {
        this.currentTab = 1;
        this.urlValue = originalRequest.url;
        this.fileName = null;
      }
    }

  }

  selectFile(event): void {
    this.selectedFiles = event.target.files;
    this.fileName = this.selectedFiles[0].name;
  }


  onNext(): void {
    switch (this.currentTab) {
      case 0:
        this.onUploadFileNext();
        break;
      case 1:
        this.onPasteUrlNext();
        break;
    }
  }

  private onPasteUrlNext() {
    this.urlForm.markAllAsTouched();
    const input = this.urlForm.get('urlControl');
    if (input.errors == null || input.errors.length == 0) {
      const url = this.urlInput.nativeElement.value;
      const model = new UrlImageScanRequest(url);

      this.dialogRef.close(model);
    }
  }

  private onUploadFileNext() {
    this.fileForm.markAllAsTouched();
    const input = this.fileForm.get('fileControl');
    if (input.errors == null || input.errors.length == 0) {
      const file = this.selectedFiles[0];
      const dialog = this.dialogRef;

      if (file.type == 'application/pdf') {
        this.api.getPdf(file).toPromise().then((blob) => {
          this.readContents(blob, file.name)
        });
      } else {
        this.readContents(file, file.name)
      }
    }
  }

  private readContents(source: File | Blob, name: string | null) {
    const reader = new FileReader();
    reader.onload = (event) => {
      const rawContent = event.target.result;
      if (typeof rawContent === 'string' || rawContent instanceof String) {
        const model = new FileImageScanRequest(rawContent as string, name);
        this.dialogRef.close(model);
      } else {
        this.dialogRef.close();
      }
    };

    reader.readAsDataURL(source);
  }

  onTabChanged(tabIndex: number): void {
    this.currentTab = tabIndex;
  }

}
