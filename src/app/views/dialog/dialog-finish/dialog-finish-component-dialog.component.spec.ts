import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogFinishComponentDialog } from './dialog-finish-component-dialog.component';

describe('DialogFinishComponent', () => {
  let component: DialogFinishComponentDialog;
  let fixture: ComponentFixture<DialogFinishComponentDialog>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DialogFinishComponentDialog ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogFinishComponentDialog);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
