import {Component, OnInit} from '@angular/core';
import {MatDialogRef} from '@angular/material/dialog';
import {Router} from '@angular/router';
import {ImageGeneratorService} from '../../../services/image-generator/image-generator.service';
import {CacheService} from '../../../services/cache/cache.service';
import {ExportService} from '../../../services/export/export.service';
import {ScanService} from '../../../services/scan/scan.service';
import {CachePoint} from '../../../model/CachePoint';
import {HeatmapModel} from '../../../model/HeatmapModel';

/**
 * Export dialog shown after gaze scanning is done.
 */
@Component({
  selector: 'app-dialog-finish',
  templateUrl: './dialog-finish-component-dialog.component.html',
  styleUrls: ['./dialog-finish-component-dialog.component.css']
})
export class DialogFinishComponentDialog implements OnInit {
  onlyHeatmap: boolean = false;
  exportCsv: boolean = false;

  constructor(
    private dialogRef: MatDialogRef<DialogFinishComponentDialog>,
    private router: Router,
    private imageGenerator: ImageGeneratorService,
    private cacheService: CacheService,
    private exportService: ExportService,
    private scanService: ScanService
  ) {
  }

  ngOnInit(): void {
  }

  /**
   * Iterate over all of items and download them
   */
  onDownload() {
    this.scanService.scanRequestArray.forEach((scanRequest) => {
      const bounds = scanRequest.clipBounds;

      if (bounds !== null) { // null in case if user never opened tab with that image
        console.log(bounds)
        console.log(scanRequest.image.width)
        console.log(scanRequest.image.height)

        const allPoints = this.cacheService.rawPoints;
        const points: Array<CachePoint> = this.cacheService.filterPoints(allPoints, scanRequest.clipBounds, scanRequest.id());
        const heatmap: HeatmapModel = this.cacheService.toHeatMap(points, null, scanRequest.id());
        const image: string = this.imageGenerator.fromImg(scanRequest.image, heatmap, scanRequest.clipBounds, this.onlyHeatmap);

        const link = document.createElement('a');
        link.download = this.getImageName(scanRequest.id());
        link.href = image;
        link.click();
        link.remove();

        if (this.exportCsv) {
          const csv = this.exportService.generateExport(points);

          const fileType = 'text/csv';
          const blob = new Blob([csv], {type: fileType});
          const a = document.createElement('a');
          a.download = this.getCsvName(scanRequest.id());
          a.href = URL.createObjectURL(blob);
          a.dataset.downloadurl = [fileType, a.download, a.href].join(':');
          a.style.display = 'none';
          document.body.appendChild(a);
          a.click();
          document.body.removeChild(a);
          setTimeout(() => {
            URL.revokeObjectURL(a.href);
          }, 1500);
        }
      }
    });
  }

  /**
   * Name for image that is exported
   */
  private getImageName(requestId: number | null) {
    if (requestId === null) {
      return `heatmap-export.png`;
    } else {
      return `heatmap-export-${requestId}.png`;
    }
  }

  /**
   * Name for CSV file that is export
   */
  private getCsvName(requestId: number | null) {
    if (requestId === null) {
      return `csv-export.csv`;
    } else {
      return `csv-export-${requestId}.csv`;
    }
  }

}
