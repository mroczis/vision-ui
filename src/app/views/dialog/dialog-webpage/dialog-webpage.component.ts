import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {MatDialog, MatDialogRef} from '@angular/material/dialog';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ScanRequest, WebScanRequest} from '../../../model/ScanRequest';
import {urlValidator} from '../../../util/ValidatorsExt';
import {ScanService} from '../../../services/scan/scan.service';
import {DialogImageComponentDialog} from '../dialog-image/dialog-image.component';
import {Router} from '@angular/router';
import {GazeService} from '../../../services/gaze/gaze.service';
import {Route} from '../../../enums/Route';

/**
 * Insert website URL button that will present dialog
 */
@Component({
  selector: 'app-dialog-webpage',
  templateUrl: './dialog-webpage.component.html',
  styleUrls: ['./dialog-webpage.component.css']
})
export class DialogWebpageComponent implements OnInit {

  constructor(
    public dialog: MatDialog,
    private router: Router,
    private scan: ScanService,
    private gazeService: GazeService,
  ) {
  }

  ngOnInit(): void {
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(DialogWebpageComponentDialog, {
      width: '440px',
    });

    dialogRef.afterClosed().subscribe(result => {
      let validatedResult: ScanRequest | null = null;
      if (result instanceof WebScanRequest) {
        validatedResult = result;
      }

      this.scan.scanRequest = validatedResult;
      if (validatedResult instanceof WebScanRequest) {
        this.gazeService.enabled = true;
        this.router.navigate([Route.SCAN]);
      }
    });
  }
}


@Component({
  selector: 'app-dialog-webpage-dialog',
  templateUrl: './dialog-webpage.component-dialog.html',
  styleUrls: ['./dialog-webpage.component-dialog.css'],
})
// tslint:disable-next-line:component-class-suffix
export class DialogWebpageComponentDialog {

  @ViewChild('url') urlInput: ElementRef;

  urlForm = new FormGroup({
    urlControl: new FormControl('', [
      Validators.required,
      urlValidator()
    ]),
  });

  constructor(
    private dialogRef: MatDialogRef<DialogImageComponentDialog>
  ) {
  }


  onNext(): void {
    this.urlForm.markAllAsTouched();
    const input = this.urlForm.get('urlControl');
    if (input.errors == null || input.errors.length == 0) {
      let url = this.urlInput.nativeElement.value;
      if (!url.startsWith("http")) {
        url = `https://${url}`
      }
      const model = new WebScanRequest(url);
      this.dialogRef.close(model);
    }
  }

}
