import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {MatDialog, MatDialogRef} from '@angular/material/dialog';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {UrlImageScanRequest} from '../../../model/ScanRequest';
import {ScanService} from '../../../services/scan/scan.service';
import {DialogImageComponentDialog} from '../dialog-image/dialog-image.component';
import {Router} from '@angular/router';
import {GazeService} from '../../../services/gaze/gaze.service';
import {Route} from '../../../enums/Route';
import {ApiService} from '../../../services/api/api.service';

/**
 * Insert code button that will present dialog
 */
@Component({
  selector: 'app-dialog-code',
  templateUrl: './dialog-code.component.html',
  styleUrls: ['./dialog-code.component.css']
})
export class DialogCodeComponent implements OnInit {

  constructor(
    public dialog: MatDialog,
    private router: Router,
    private scan: ScanService,
    private gazeService: GazeService,
  ) {
  }

  ngOnInit(): void {
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(DialogCodeComponentDialog, {
      width: '440px',
    });

    dialogRef.afterClosed().subscribe((result) => {
      console.log(result)
      if (result !== null) {
        this.scan.scanRequests = result.items.map((it) => {
          return new UrlImageScanRequest(it.url, it.id, it.name);
        });
        this.scan.requestId = result.startItemId;

        this.gazeService.enabled = true;
        this.router.navigate([Route.SCAN]);
      }
    });
  }
}


@Component({
  selector: 'app-dialog-code-dialog',
  templateUrl: './dialog-code.component-dialog.html',
  styleUrls: ['./dialog-code.component-dialog.css'],
})
// tslint:disable-next-line:component-class-suffix
export class DialogCodeComponentDialog {

  @ViewChild('code') codeInput: ElementRef;

  codeForm = new FormGroup({
    codeControl: new FormControl('', [
      Validators.required
    ]),
  });

  constructor(
    private dialogRef: MatDialogRef<DialogImageComponentDialog>,
    private api: ApiService
  ) {
  }

  onNext(): void {
    this.codeForm.markAllAsTouched();
    const input = this.codeForm.get('codeControl');
    if (input.errors == null || input.errors.length == 0) {
      const code = this.codeInput.nativeElement.value;

      this.api.getBundle(code).toPromise()
        .then((bundle) => {
          this.dialogRef.close(bundle);
        })
        .catch((error) => {
          console.log(error);
          input.setErrors({'invalid': true});
        });

    }
  }

}
