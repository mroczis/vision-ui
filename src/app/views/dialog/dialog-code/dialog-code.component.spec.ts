import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogWebpageComponent } from './dialog-code.component';

describe('DialogWebpageComponent', () => {
  let component: DialogWebpageComponent;
  let fixture: ComponentFixture<DialogWebpageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DialogWebpageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogWebpageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
