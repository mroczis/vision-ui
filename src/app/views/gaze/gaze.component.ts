import {AfterViewInit, Component, ElementRef, Input, NgZone, OnDestroy, ViewChild} from '@angular/core';

import {create, Heatmap} from 'heatmap.js';
import {GazeService} from '../../services/gaze/gaze.service';
import {CalibrationService} from '../../services/calibration/calibration.service';
import {DotPosition} from '../../enums/DotPosition';
import {ScanningState} from '../../enums/ScanningState';
import {Observable, Subscription} from 'rxjs';
import {GazeScanningState} from '../../enums/GazeScanningState';
import {Rect} from '../../model/Rect';
import {CacheService} from '../../services/cache/cache.service';
import {map} from 'rxjs/operators';

/**
 * Gaze component handles drawing of current state of the system.
 * It also draws overlying heat maps on top of current image.
 */
@Component({
  selector: 'app-gaze',
  templateUrl: './gaze.component.html',
  styleUrls: ['./gaze.component.css']
})
export class GazeComponent implements AfterViewInit, OnDestroy {

  currentState: ScanningState = ScanningState.LOADING;

  private canvasRect: Rect | null = null;
  private heatmapLib: Heatmap<'value', 'x', 'y'> | null = null;

  private stateHandle: Subscription = this.gaze.state.subscribe((it) => {
    console.log(`Current state : ${ScanningState[it]}`);
    this.ngZone.run(() => {
      this.currentState = it;
    });
  });

  private heatmapHandle: Subscription = this.cache.points.subscribe((it) => {
    if (this.heatmapLib !== null && this.canvasRect !== null) {
      const heatmapModel = this.cache.toHeatMap(it, this.canvasRect);

      this.heatmapLib.setData(heatmapModel);
      this.heatmapLib.setDataMax(heatmapModel.max);
    }
  });

  @Input() set canvasPos(canvasPos: Rect | null | undefined) {
    if (canvasPos instanceof Rect && canvasPos.width > 0 && canvasPos.height > 0) {
      const c = <HTMLDivElement> this.canvasesRef.nativeElement;
      while (c.firstChild) {
        c.removeChild(c.lastChild);
      }

      c.style.width = `${canvasPos.width}px`;
      c.style.height = `${canvasPos.height}px`;
      c.style.top = `${canvasPos.top}px`;
      c.style.left = `${canvasPos.left}px`;

      this.canvasRect = canvasPos;
      this.heatmapLib = create({container: c});

    }
  }

  @ViewChild('canvases')
  canvasesRef: ElementRef;

  constructor(
    public gaze: GazeService,
    private calibration: CalibrationService,
    private cache: CacheService,
    private ngZone: NgZone
  ) {
    navigator.mediaDevices.getUserMedia({'video': true})
      .then(_ => {
        this.gaze.hasVideoStream = true
      }).catch(_ => {
        this.gaze.hasVideoStream = false
    });
  }

  ngAfterViewInit(): void {
    this.gaze.enabled = true;
  }

  ngOnDestroy(): void {
    this.stateHandle.unsubscribe();
    this.heatmapHandle.unsubscribe();
    this.gaze.enabled = false;
  }

  onResize(_: any) {
    this.calibration.clear();
  }

  onDotClick(dot: DotPosition) {
    this.calibration.onDotClick(dot);
  }

  getClicks(dot: DotPosition) : Observable<string> {
    return this.calibration.getDotClicks(dot).pipe(
      map((amount) => `dot-clicks-${amount}`)
    );
  }

  get dot() {
    return DotPosition;
  }

  get state() {
    return ScanningState;
  }

  onStart() {
    if (this.gaze.scanningState == GazeScanningState.READY || this.gaze.scanningState == GazeScanningState.PAUSED) {
      this.gaze.scanningState = GazeScanningState.RUNNING;
    }
  }
}
