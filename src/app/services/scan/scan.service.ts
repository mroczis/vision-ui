import {Injectable} from '@angular/core';
import {ScanRequest, UrlImageScanRequest, WebScanRequest} from '../../model/ScanRequest';
import {BehaviorSubject, combineLatest, Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {CachedScanRequest} from '../../model/CachedScanRequest';
import {ApiService} from '../api/api.service';
import {Rect} from '../../model/Rect';

/**
 * Holds items for gaze scan and currently visible one
 */
@Injectable({
  providedIn: 'root'
})
export class ScanService {

  /**
   * Collection of items that we want to track gaze
   */
  private _requests: BehaviorSubject<Array<CachedScanRequest>> = new BehaviorSubject([]);

  /**
   * Current item that should be displayed id
   */
  private _requestId: BehaviorSubject<number | null> = new BehaviorSubject(null);

  /**
   * Current item model
   */
  private _scanRequest: Observable<CachedScanRequest | null> = combineLatest([this._requests, this._requestId]).pipe(
    map(([requests, selectedId]) => {
      const result = requests.find((it) => it.id() == selectedId);

      if (result === undefined) {
        return null;
      } else {
        return result;
      }
    }));

  constructor(
    private api: ApiService
  ) {

  }

  get scanRequestObservable(): Observable<CachedScanRequest | null> {
    return this._scanRequest;
  }

  get requestId(): number | null {
    return this._requestId.value;
  }

  set requestId(selectedId: number | null) {
    this._requestId.next(selectedId);
  }

  get scanRequestArray(): Array<CachedScanRequest> {
    return this._requests.value;
  }

  set scanRequest(req: ScanRequest | null) {
    if (req !== null) {
      this.scanRequests = [req];
    } else {
      this.scanRequests = [];
    }

    this._requestId.next(req == null ? null : req.id());
  }

  set scanRequests(req: Array<ScanRequest>) {
    const cachedRequests = req.map((model) => {
      let url;
      if (model instanceof WebScanRequest) {
        url = this.api.getScreenshot(model.src(), window.innerWidth);
      } else if (model instanceof UrlImageScanRequest) {
        url = this.api.getImage(model.src());
      } else {
        url = model.src();
      }

      const image = new Image();
      image.src = url;
      image.crossOrigin = 'anonymous';
      return new CachedScanRequest(image, model);
    });

    this._requests.next(cachedRequests);
  }

  /**
   * Calculates absolute position of image on canvas
   *
   * @param parentWidth   width of parent were image is positioned
   * @param parentHeight  height of parent were image is positioned
   * @param imageWidth    width of image
   * @param imageHeight   height of image
   * @param offsetTop     absolute offset from top of the screen
   * @param offsetLeft    absolute offset from left of the screen
   */
  calculateImagePosition(
    parentWidth: number,
    parentHeight: number,
    imageWidth: number,
    imageHeight: number,
    offsetTop: number,
    offsetLeft: number
  ): Rect {
    if (imageWidth > parentWidth) {
      const bot = imageHeight * parentWidth / imageWidth;
      return new Rect(offsetLeft, offsetTop, offsetLeft + parentWidth, offsetTop + bot);
    } else {
      const marginLR = (parentWidth - imageWidth) / 2;
      const height = parentHeight - offsetTop;
      let marginTB = 0;
      if (imageHeight < height) {
        marginTB = Math.floor((height - imageHeight) / 2);
      }

      return new Rect(
        offsetLeft + marginLR,
        offsetTop + marginTB,
        offsetLeft + marginLR + imageWidth,
        offsetTop + marginTB + imageHeight);
    }
  }

}
