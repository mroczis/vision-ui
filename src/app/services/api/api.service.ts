import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Bundle} from '../../model/Bundle';
import {environment} from '../../../environments/environment';

/**
 * Service used to maintain non-secured connection to server.
 */
@Injectable({
  providedIn: 'root'
})
export class ApiService {

  private BASE_URL = environment.apiUrl;

  constructor(
    private http: HttpClient,
  ) {
  }

  getScreenshot(url: string, width: number): string {
    const fullUrl = new URL('screenshot', this.BASE_URL);
    fullUrl.searchParams.set('url', url);
    fullUrl.searchParams.set('width', width.toString(10));

    return fullUrl.toString();
  }

  getImage(url: string): string {
    const fullUrl = new URL('image', this.BASE_URL);
    fullUrl.searchParams.set('url', url);

    return fullUrl.toString();
  }

  getBundle(key: string): Observable<Bundle> {
    return this.http.get<Bundle>(this.path(`bundle/${key}`),
      {responseType: 'json'}
    );
  }


  getPdf(file: File): Observable<Blob> {
    const form = new FormData();
    form.append('pdf', file);

    return this.http.post(this.path('pdf'), form, {
      responseType: 'blob',
      headers: {
        'Accept': 'image/*'
      },
    });

  }

  private path(path: string): string {
    return new URL(path, this.BASE_URL).toString();
  }

}
