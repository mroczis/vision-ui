import {Injectable} from '@angular/core';
import {BehaviorSubject, combineLatest, Observable} from 'rxjs';
import {ScanningState} from '../../enums/ScanningState';
import {GazeScanningState} from '../../enums/GazeScanningState';
import {map} from 'rxjs/operators';
import {Visibility} from '../../enums/Visibility';

/**
 * Holds current state of system
 */
@Injectable({
  providedIn: 'root'
})
export class GazeService {

  /**
   * Scanning is enabled when correct component that is able to handle scanning
   * is visible. Otherwise this is false
   */
  private readonly _enabled: BehaviorSubject<Boolean>;
  private readonly _hasCamera: BehaviorSubject<Boolean>;
  private readonly _loaded: BehaviorSubject<Boolean>;
  private readonly _imageLoaded: BehaviorSubject<Boolean>;
  private readonly _faceDetected: BehaviorSubject<Boolean>;
  private readonly _calibrated: BehaviorSubject<Boolean>;
  private readonly _scanningState: BehaviorSubject<GazeScanningState>;
  private readonly _state: Observable<ScanningState>;
  private readonly _targetVisibility: Observable<Visibility>;

  constructor() {
    this._enabled = new BehaviorSubject(false);
    this._hasCamera = new BehaviorSubject<Boolean>(false);
    this._loaded = new BehaviorSubject(false);
    this._imageLoaded = new BehaviorSubject(false);
    this._faceDetected = new BehaviorSubject(false);
    this._calibrated = new BehaviorSubject(false);
    this._scanningState = new BehaviorSubject(GazeScanningState.READY);

    this._state = combineLatest([this._loaded, this._hasCamera, this._imageLoaded, this._faceDetected, this._calibrated, this._scanningState]).pipe(
      map(([loaded, hasCamera, imageLoaded, faceDetected, calibrated, gaze]) => {
        if (!hasCamera) {
          return ScanningState.NO_CAMERA;
        } else if (!loaded || !imageLoaded) {
          return ScanningState.LOADING;
        } else if (!faceDetected && gaze !== GazeScanningState.FINISHED) {
          return ScanningState.SEARCHING_FOR_FACE;
        } else if (!calibrated && gaze !== GazeScanningState.FINISHED) {
          return ScanningState.CALIBRATING;
        } else if (gaze === GazeScanningState.READY) {
          return ScanningState.READY;
        } else if (gaze === GazeScanningState.FINISHED) {
          return ScanningState.FINISHED;
        } else if (gaze === GazeScanningState.PAUSED) {
          return ScanningState.PAUSED;
        } else if (gaze === GazeScanningState.RUNNING) {
          return ScanningState.SCANNING;
        } else {
          throw 'Undefined state';
        }
      })
    );

    this._targetVisibility = this._state.pipe(
      map((state) => {
        if (state === ScanningState.FINISHED || state === ScanningState.SCANNING) {
          return Visibility.VISIBLE;
        } else {
          return Visibility.HIDDEN;
        }
      })
    );
  }

  // Loaded
  set webgazerLoaded(loaded: Boolean) {
    if (this._loaded.value !== loaded) {
      this._loaded.next(loaded);
    }
  }

  // Image loaded
  set imageLoaded(loaded: Boolean) {
    if (this._imageLoaded.value !== loaded) {
      this._imageLoaded.next(loaded);
    }
  }

  // Face found
  get faceDetected(): Boolean {
    return this._faceDetected.value;
  }

  set faceDetected(detected: Boolean) {
    if (this.faceDetected !== detected) {
      this._faceDetected.next(detected);
    }
  }

  // Calibrated
  get calibrated(): Boolean {
    return this._calibrated.value;
  }

  set calibrated(calibrated: Boolean) {
    if (this.calibrated !== calibrated) {
      this._calibrated.next(calibrated);

      if (calibrated === true) {
        this.scanningState = GazeScanningState.READY;
      }
    }
  }

  // Scanning state
  get scanningState(): GazeScanningState {
    return this._scanningState.value;
  }

  set scanningState(state: GazeScanningState) {
    if (this.scanningState !== state) {
      this._scanningState.next(state);
    }
  }


  get state(): Observable<ScanningState> {
    return this._state;
  }

  // Enabled
  set enabled(enabled: Boolean) {
    if (this.enabled !== enabled) {
      this._enabled.next(enabled);
    }
  }

  get enabled(): Boolean {
    return this._enabled.value;
  }

  get enabledObservable(): Observable<Boolean> {
    return this._enabled;
  }

  /**
   * Visibility of image elements that are background for gaze tracking
   */
  get targetVisibility(): Observable<Visibility> {
    return this._targetVisibility;
  }

  /**
   * Set if camera hardware can be accessed
   * @param hasStream true if so
   */
  set hasVideoStream(hasStream: boolean) {
    if (this._hasCamera.value !== hasStream) {
      this._hasCamera.next(hasStream);
    }
  }

}
