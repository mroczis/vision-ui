import { TestBed } from '@angular/core/testing';

import { GazeService } from './gaze.service';

describe('GazeService', () => {
  let service: GazeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GazeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
