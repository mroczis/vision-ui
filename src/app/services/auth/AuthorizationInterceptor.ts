import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {AuthService} from './auth.service';

/**
 * Adds authorization header to outgoing requests if it's filled.
 */
@Injectable()
export class AuthorizationInterceptor implements HttpInterceptor {

  constructor(public auth: AuthService) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const header = this.auth.baseAuthHeader;
    if (header !== null) {
      req = req.clone({
        setHeaders: {
          Authorization: `Basic ${this.auth.baseAuthHeader}`
        }
      });

      return next.handle(req);
    } else {
      return next.handle(req);
    }
  }

}
