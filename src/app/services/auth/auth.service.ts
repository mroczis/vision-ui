import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {catchError, map} from 'rxjs/operators';
import {Credentials} from '../../model/Credentials';
import {Bundle} from '../../model/Bundle';
import {PagedBundle} from '../../model/PagedBundle';
import {OutgoingBundle} from '../../model/OutgoingBundle';

/**
 * Service used to maintain secured connection to server.
 */
@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private BASE_URL = environment.apiUrl;
  private credentials: Credentials | null = null;

  constructor(
    private http: HttpClient,
  ) {
  }

  login(username: string, password: string): Observable<boolean> {
    this.credentials = new Credentials(username, password)

    return this.http.get(
      this.path(`login`),
      {responseType: 'json', observe: 'response'}
    ).pipe(map(() => true),
      catchError(() => {
        this.credentials = null
        return of(false)
      })
    );
  }

  getBundles(limit: number, offset: number, query: string): Observable<PagedBundle> {
    return this.http.get<PagedBundle>(
      this.path(`bundle?limit=${limit}&offset=${offset}&query=${query}`),
      {responseType: 'json'}
    );
  }

  getBundle(key: string): Observable<Bundle> {
    return this.http.get<Bundle>(
      this.path(`bundle/${key}`),
      {responseType: 'json'}
    );
  }

  createBundle(model: OutgoingBundle): Observable<Bundle> {
    return this.http.post<Bundle>(
      this.path(`bundle`),
      model,
      {responseType: 'json'}
    );
  }

  updateBundle(id: number, model: OutgoingBundle): Observable<Bundle> {
    return this.http.put<Bundle>(
      this.path(`bundle/${id}`),
      model,
      {responseType: 'json'}
    );
  }

  deleteBundle(id: number): Observable<boolean> {
    return this.http.delete<any>(
      this.path(`bundle/${id}`),
      {observe: 'response'}
    ).pipe(
      map(() => true),
      catchError(() => of(false))
    );
  }

  logout() {
    this.credentials = null;
  }

  private path(path: string): string {
    return new URL(path, this.BASE_URL).toString();
  }

  get baseAuthHeader(): string {
    if (this.credentials != null) {
      return btoa(`${this.credentials.username}:${this.credentials.password}`);
    } else {
      return null;
    }
  }

}
