import { Injectable } from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {Consumable} from '../../model/Consumable';

/**
 * Holds global notifications that are component independent.
 */
@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  private _message = new BehaviorSubject<Consumable<string>>(new Consumable<string>("", true))
  message = this._message.asObservable()

  constructor() { }

  setMessage(message: string) {
    this._message.next(new Consumable<string>(message))
  }


}
