import {Injectable} from '@angular/core';
import {GazeService} from '../gaze/gaze.service';
import {DotPosition} from '../../enums/DotPosition';
import {BehaviorSubject, Observable} from 'rxjs';
import {map} from 'rxjs/operators';

/**
 * Holds calibration state and exposes methods to check if system is calibrated or not.
 */
@Injectable({
  providedIn: 'root'
})
export class CalibrationService {

  private static readonly MIN_CLICKS = 2;

  private readonly clicks: BehaviorSubject<Map<DotPosition, number>> = new BehaviorSubject<Map<DotPosition, number>>(new Map());

  constructor(
    private gazeService: GazeService
  ) {
  }

  /**
   * Deletes all calibration data
   */
  clear() {
    const map = new Map()
    this.clicks.next(map);
    this.invalidateCalibrated(map);
  }

  /**
   * Enhances calibration and forces re-check if system is calibrated.
   * @param dot dot that was clicked
   */
  onDotClick(dot: DotPosition) {
    const map = this.clicks.value;
    const amount = map.get(dot);

    if (amount === undefined) {
      map.set(dot, 1);
    } else {
      map.set(dot, amount + 1);
    }

    this.clicks.next(map);
    this.invalidateCalibrated(map);
  }

  /**
   * Checks if user clicked on required amount of calibration points.
   * If so invalidates observable in GazeService
   */
  invalidateCalibrated(map: Map<DotPosition, number>) {
    const notCalibrated = Object.values(DotPosition).find((dotString: string) => {
      const dot = DotPosition[dotString];
      const clicks: number | undefined = map.get(dot);
      return clicks === undefined || clicks < CalibrationService.MIN_CLICKS;
    }) !== undefined;

    this.gazeService.calibrated = !notCalibrated;
  }

  /**
   * Tells us how many times given dot was clicked
   */
  getDotClicks(dot: DotPosition): Observable<number> {
    return this.clicks.pipe(
      map((map) => {
        const amount = map.get(dot);
        if (amount === undefined) {
          return 0;
        } else {
          return amount;
        }
      })
    );
  }
}
