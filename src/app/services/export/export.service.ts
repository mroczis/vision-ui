import { Injectable } from '@angular/core';
import {CachePoint} from '../../model/CachePoint';

/**
 * Helper service for exports generation
 */
@Injectable({
  providedIn: 'root'
})
export class ExportService {

  constructor() { }

  /**
   * Generates export string
   * @param points points
   */
  generateExport(points : Array<CachePoint>) : string {
    return points.map(this.generateExportLine)
      .reduce((prev, curr) => `${prev}\r\n${curr}`);
  }

  /**
   * Converts point to one line of export
   * @param point point
   */
  generateExportLine(point: CachePoint) : string {
    return `${point.timestamp};${point.x};${point.y}`
  }

}
