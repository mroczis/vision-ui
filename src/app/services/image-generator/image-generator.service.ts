import {Injectable} from '@angular/core';
import {create} from 'heatmap.js';
import {HeatmapModel} from '../../model/HeatmapModel';
import {Rect} from '../../model/Rect';

/**
 * Helper service that will download image
 */
@Injectable({
  providedIn: 'root'
})
export class ImageGeneratorService {

  private readonly TEMP_EXPORT_ROOT = 'export-root';

  constructor() {
  }


  /**
   * Combines image from {@param img}, and heatmap from {@param heatmapModel} into one image and returns it
   *
   * @param img instance of img element holding background image
   * @param heatmapModel points that should be be drawn as a heatmap
   * @param rect rectangle defining size of target canvas
   * @param heatmapOnly if true will export only heatmap
   */
  fromImg(
    img: HTMLImageElement,
    heatmapModel: HeatmapModel,
    rect: Rect,
    heatmapOnly: boolean
  ): string {
    const root : HTMLDivElement = document.createElement('div');
    root.id = this.TEMP_EXPORT_ROOT;
    root.style.display = 'none';
    root.style.width = `${rect.width}px`
    root.style.height = `${rect.height}px`
    document.body.appendChild(root);

    const dest: HTMLCanvasElement = document.createElement('canvas');
    dest.width = rect.width;
    dest.height = rect.height;
    const context = dest.getContext('2d');

    // Background image
    if (!heatmapOnly) {
      img.width = rect.width
      img.height = rect.height
      root.appendChild(img);
    }

    // Heatmap canvas
    const heatmapLib = create({container: root});
    heatmapLib.setData(heatmapModel);
    heatmapLib.setDataMax(heatmapModel.max);

    // Draw all of them into one canvas
    root.childNodes.forEach((child) => {
      if (child instanceof HTMLCanvasElement || child instanceof HTMLImageElement) {
        context.drawImage(<CanvasImageSource> child, 0, 0, rect.width, rect.height);
      }
    });

    const url: string = dest.toDataURL('image/png');

    root.remove();
    return url;
  }
}
