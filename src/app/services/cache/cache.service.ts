import {Injectable} from '@angular/core';
import {Point, Rounding} from '../../model/Point';
import {HeatmapModel} from '../../model/HeatmapModel';
import {WeightedPoint} from '../../model/WeightedPoint';
import {BehaviorSubject, Observable} from 'rxjs';
import {Rect} from '../../model/Rect';
import {CachePoint} from '../../model/CachePoint';
import {IPoint} from '../../model/IPoint';
import {ScanService} from '../scan/scan.service';

type CoordinateX = number
type CoordinateY = number
type Weight = number

/**
 * Caches all points that represent position where user looked
 */
@Injectable({
  providedIn: 'root'
})
export class CacheService {

  private readonly DOWNSCALE_RATIO = 20;

  private _points: BehaviorSubject<Array<CachePoint>> = new BehaviorSubject([]);

  constructor(
    private scanService: ScanService
  ) {
  }

  /**
   * Adds point to cache
   * @param point     point
   * @param requestId currently visible image id
   */
  add(point: IPoint, requestId: number | null = this.scanService.requestId) {
    const updated = this._points.value;
    updated.push(new CachePoint(point, requestId));

    this._points.next(updated);
  }

  /**
   * Clears all cached data
   */
  clear() {
    this._points.next([]);
  }

  /**
   * Filters points and adjusts their coordinates according to {@param clipBounds}.
   * Those that are not in bounds are removed. And those that are inside are remapped to positions
   * whitin {@param clipBounds}.
   *
   * @param data points
   * @param requestId id of scan request (unique per image)
   * @param clipBounds rectangle in which output points must be
   */
  filterPoints(
    data: Array<CachePoint>,
    clipBounds: Rect | null,
    requestId: number | null = this.scanService.requestId,
  ): Array<CachePoint> {
    if (clipBounds !== null) {
      const topLeft = new Point(clipBounds.left, clipBounds.top);
      return data
        .filter((point) => clipBounds.contains(point) && point.requestId == requestId)
        .map((point) => point.minus(topLeft));
    } else {
      return data;
    }
  }

  /**
   * Converts cached points into Heatmap object, optionally merging several points into one depending on
   * {@param downscale}. Passing 1 will yield in no downscaling, passing "10" will result that
   * points with x coordinate from "0" to "10" will be merged to x-coordinate "5" (mean is applied)
   *
   * @param data array of points that should be processed
   * @param requestId id of scan request (unique per image)
   * @param clipBounds rect defining heatmap bounds, coordinates [0,0] define top-left corner
   * @param downscale from 1 to +inf, amount of pixels that should be merged into one point
   */
  toHeatMap(
    data: Array<CachePoint>,
    clipBounds: Rect | null,
    requestId: number | null = this.scanService.requestId,
    downscale: number = this.DOWNSCALE_RATIO
  ): HeatmapModel {
    if (downscale < 1) {
      throw 'Downscale parameter must be greater than 1';
    }

    const hits: Map<CoordinateX, Map<CoordinateY, Weight>> = new Map();
    const filtered: Array<IPoint> = this.filterPoints(data, clipBounds, requestId);

    filtered.forEach((point) => {
      const pointDown = point.divide(downscale, Rounding.DOWN);
      const pointUp = point.divide(downscale, Rounding.UP);
      const pointMean = pointDown.mean(pointUp).multiply(downscale);

      let hits2: Map<CoordinateY, Weight> = hits.get(pointMean.x);
      if (hits2 === undefined) {
        hits2 = new Map();
        hits.set(pointMean.x, hits2);
      }

      let weight = hits2.get(pointMean.y);
      if (weight === undefined) {
        weight = 0;
      }

      hits2.set(pointMean.y, weight + 1);
    });

    const points: Array<WeightedPoint> = [];
    let maxWeight: number = 0;

    hits.forEach((val: Map<CoordinateY, Weight>, x: CoordinateX) => {
      val.forEach((weight: Weight, y: CoordinateY) => {
        if (x >= 0 && y >= 0) {
          points.push(new WeightedPoint(x, y, weight));
          if (maxWeight < weight) {
            maxWeight = weight;
          }
        }
      });
    });

    return new HeatmapModel(points, maxWeight);
  }

  /**
   * Observable cached points
   */
  get points(): Observable<Array<CachePoint>> {
    return this._points;
  }

  /**
   * Cached points
   */
  get rawPoints(): Array<CachePoint> {
    return this._points.value;
  }

}
