import {NgModule} from '@angular/core';
import {ScanComponent} from './views/scan/scan.component';
import {RouterModule, Routes} from '@angular/router';
import {DashboardComponent} from './views/dashboard/dashboard.component';
import {Route} from './enums/Route';
import {AdminComponent} from './views/admin/login/admin.component';
import {BundlesComponent} from './views/admin/bundles/bundles.component';
import {BundleComponent} from './views/admin/bundle/bundle.component';

const routes: Routes = [
  {path: '', redirectTo: Route.DASHBOARD, pathMatch: 'full'},
  {path: Route.DASHBOARD, component: DashboardComponent},
  {path: Route.SCAN, component: ScanComponent},
  {path: Route.ADMIN_LOGIN, component: AdminComponent},
  {path: Route.ADMIN_BUNDLES, component: BundlesComponent},
  {path: Route.ADMIN_BUNDLE, component: BundleComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
