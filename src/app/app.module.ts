import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatButtonModule} from '@angular/material/button';
import {DialogWebpageComponent, DialogWebpageComponentDialog} from './views/dialog/dialog-webpage/dialog-webpage.component';
import {MatDialogModule} from '@angular/material/dialog';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {DialogImageComponent, DialogImageComponentDialog} from './views/dialog/dialog-image/dialog-image.component';
import {MatTabsModule} from '@angular/material/tabs';
import {MatIconModule} from '@angular/material/icon';
import {ScanComponent} from './views/scan/scan.component';
import {AppRoutingModule} from './app-routing.module';
import {DashboardComponent} from './views/dashboard/dashboard.component';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {GazeComponent} from './views/gaze/gaze.component';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {DialogFinishComponentDialog} from './views/dialog/dialog-finish/dialog-finish-component-dialog.component';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {WebgazerComponent} from './views/webgazer/webgazer.component';
import {CommonModule} from '@angular/common';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {DialogCodeComponent, DialogCodeComponentDialog} from './views/dialog/dialog-code/dialog-code.component';
import { AdminComponent } from './views/admin/login/admin.component';
import { BundlesComponent } from './views/admin/bundles/bundles.component';
import { BundleComponent } from './views/admin/bundle/bundle.component';
import {MatTableModule} from '@angular/material/table';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatSelectModule} from '@angular/material/select';
import {MatMenuModule} from '@angular/material/menu';
import {AuthorizationInterceptor} from './services/auth/AuthorizationInterceptor';
import {MatTooltipModule} from '@angular/material/tooltip';

@NgModule({
  declarations: [
    AppComponent,
    DialogWebpageComponent,
    DialogWebpageComponentDialog,
    DialogImageComponent,
    DialogImageComponentDialog,
    ScanComponent,
    DashboardComponent,
    GazeComponent,
    DialogFinishComponentDialog,
    WebgazerComponent,
    DialogCodeComponent,
    DialogCodeComponentDialog,
    AdminComponent,
    BundlesComponent,
    BundleComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    ReactiveFormsModule,
    MatTabsModule,
    MatIconModule,
    AppRoutingModule,
    MatToolbarModule,
    MatSnackBarModule,
    MatProgressBarModule,
    MatSlideToggleModule,
    MatTableModule,
    FormsModule,
    CommonModule,
    HttpClientModule,
    MatPaginatorModule,
    MatSelectModule,
    MatMenuModule,
    MatTooltipModule
  ],
  bootstrap: [AppComponent],
  providers:[
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthorizationInterceptor,
      multi: true
    }
  ]
})
export class AppModule {
}
