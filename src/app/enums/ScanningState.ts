/**
 * Current state of whole system
 */
export enum ScanningState {
  /**
   * No camera detected or permission denied
   */
  NO_CAMERA,
  /**
   * Library data are currently being loaded
   */
  LOADING,
  /**
   * Library is loaded, face is not detected
   */
  SEARCHING_FOR_FACE,
  /**
   * Face found, calibration is required
   */
  CALIBRATING,
  /**
   * Ready for everything, user confirmation required
   */
  READY,
  /**
   * Gaze scanning is ongoing
   */
  SCANNING,
  /**
   * User manually paused the scanning
   */
  PAUSED,
  /**
   * Done results are here
   */
  FINISHED,
}
