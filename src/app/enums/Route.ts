/**
 * All routes that are supported by our application
 */
export enum Route {
  DASHBOARD = "home",
  SCAN = "scan",
  ADMIN_LOGIN = "admin/login",
  ADMIN_BUNDLES = "admin/bundles",
  ADMIN_BUNDLE = "admin/bundles/:key"
}
