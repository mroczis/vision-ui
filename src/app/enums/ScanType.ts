/**
 * Tells us what are we currently scanning
 */
export enum ScanType {
  WEBSITE,
  IMAGE_FILE,
  IMAGE_URL,
}
