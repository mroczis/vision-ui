/**
 * HTML visibility enum
 */
export enum Visibility {
  HIDDEN = "hidden",
  VISIBLE = "visible"
}
