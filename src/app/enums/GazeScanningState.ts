/**
 * Gaze scanning enum - determines if scanning process should be active or not
 */
export enum GazeScanningState {
  /**
   * Scanning did not start and it's ready
   */
  READY,
  /**
   * Scanning is ongoing
   */
  RUNNING,
  /**
   * Paused by user
   */
  PAUSED,
  /**
   * User successfully finished scanning (pressed stop button)
   */
  FINISHED,
}
